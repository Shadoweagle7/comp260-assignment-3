﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointSpawner : MonoBehaviour {
    public float timer = 3f;
    public PointBehaviour PointPrefab;
    public int nPoint = 3;
    public Rect spawnRect;
    public int count = 0;
    // Use this for initialization
    public Rigidbody2D rigidbodyPlayer;

    private float[] sizes;

    void Start()
    {
        InvokeRepeating("Spawner", timer, timer);

        // Randomize sizes for the diamonds which will give you points. Smaller diamonds
        // are harder to catch, so they are worth more.
        this.sizes = new float[100];

        // 10% chance of getting a small diamond - 20 points
        for (int i = 0; i < 10; i++)
        {
            this.sizes[i] = 1f;
        }

        // 20% chance of getting a medium diamond - 15 points
        for (int i = 10; i < 30; i++)
        {
            this.sizes[i] = 1.5f;
        }

        // 30% chance of getting a large diamond - 10 points
        for (int i = 30; i < 60; i++)
        {
            this.sizes[i] = 2f;
        }

        // 40% chance of getting a very large diamond - 5 points
        for (int i = 60; i < 100; i++)
        {
            this.sizes[i] = 2.5f;
        }
    }

    //spawns in falling points that will occur once every 'timer' has passed above the player
    void Spawner()
    {
        //taken from week 4 practical
        // instantiate a box
        PointBehaviour point = Instantiate(PointPrefab);

        int randomSize = Random.Range(0, 100);

        point.ScoreReward = (int)(30 - (this.sizes[randomSize] * 10)); // Larger diamonds give smaller points
        point.transform.localScale *= new Vector2(this.sizes[randomSize], this.sizes[randomSize]);

        // attach to this object in the hierarchy
        point.transform.parent = transform;
        // give the point a name and number
        point.gameObject.name = "Point " + count++;
        // move the point to a random position within
        // the spawn rectangle at 13 spaces above the player
        float x = spawnRect.xMin +
        Random.value * spawnRect.width;
        float y = rigidbodyPlayer.position.y + 15;//spawnRect.yMin +
        //Random.value * spawnRect.height;
        point.transform.position = new Vector2(x, y);
    }
    void OnDrawGizmos()
    {
        // draw the spawning line
        //not working for some reason idk
        Gizmos.color = Color.red;
        Gizmos.DrawLine(
            new Vector2(spawnRect.xMin, rigidbodyPlayer.position.y + 15),
            new Vector2(spawnRect.xMax, rigidbodyPlayer.position.y + 15));
    }

}
