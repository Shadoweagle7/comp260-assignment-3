﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaBehaviour : MonoBehaviour {
    public Rigidbody2D spawnPoint;
    public Player player;
    public AudioClip DeathAudioClip;

	// Use this for initialization
	void Start () {
        if (player == null)
        {
            Debug.LogError("ERROR: " + this.player + " not assigned a Player target");
        }
	}
	
	// Update is called once per frame
	void Update () {

    }

    private void OnCollisionEnter2D(Collision2D collision2D)
    {
        if (collision2D.gameObject == this.player.gameObject)
        {
            //this.player.gameObject.transform.position.Set(-16.5f, 7.13f, 0);
            // Deal with death properly
        }

        // If we have time play an animation here before destroying the object

        //destroys the boxes, points and resets players position if contact is made
        if(collision2D.gameObject.name.Contains("Box"))
        {
            Destroy(collision2D.gameObject);
        } else if(collision2D.gameObject.name.Contains("Point"))
        {
            Destroy(collision2D.gameObject);
        } else if (collision2D.gameObject.name.Contains("Player"))
        {
            this.player.scoreCount = 0;
            collision2D.transform.position = spawnPoint.position;
            this.player.PlayerAudioSource.PlayOneShot(this.DeathAudioClip);
        }
        
    }
}
