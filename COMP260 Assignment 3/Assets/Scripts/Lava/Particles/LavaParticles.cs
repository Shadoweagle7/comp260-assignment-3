﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaParticles : MonoBehaviour {
    public float timer = 3f;
    public GameObject LavaPrefab;
    public GameObject LargeLavaSpash;
    public int nLava = 3;
    public Rect spawnRect;
    public int count = 0;

    // Use this for initialization
    void Start () {

    }

    private void OnCollisionEnter2D(Collision2D collision2D)
    {
        if(collision2D.gameObject.name.Contains("Box") || collision2D.gameObject.name.Contains("Point"))
        {
            //taken from week 4 practical
            // instantiate a laser
            GameObject lava = Instantiate(LavaPrefab);
            // attach to this object in the hierarchy
            //lava.transform.parent = transform;
            
            // give the box a name and number
            lava.gameObject.name = "LavaParticle " + count++;
            // move the box to a random position within
            // the spawn rectangle at 13 spaces above the player
            float x = collision2D.transform.position.x;
            float y = this.gameObject.transform.position.y;//spawnRect.yMin +
                                                      //Random.value * spawnRect.height;
            lava.transform.position = new Vector2(x, y);
        }

        int largeLavaSplashChance = Random.Range(0, 10);

        if (largeLavaSplashChance < 4)
        {
            GameObject largeLavaSplashLeft = Instantiate(this.LargeLavaSpash);
            GameObject largeLavaSplashRight = Instantiate(this.LargeLavaSpash);

            SpriteRenderer spriteRenderer = largeLavaSplashLeft.GetComponent<SpriteRenderer>();
            spriteRenderer.flipY = true;

            Vector2 lavaPosition = new Vector2(collision2D.transform.position.x,
                this.gameObject.transform.position.y);

            float randomLavaSplashSizeFactor = 2 + Random.value * 3;

            largeLavaSplashLeft.transform.position = lavaPosition;
            largeLavaSplashRight.transform.position = lavaPosition;

            largeLavaSplashRight.transform.localScale *= randomLavaSplashSizeFactor;

            largeLavaSplashLeft.name = "LavaSplashLeft";
            largeLavaSplashRight.name = "LavaSplashRight";

            //largeLavaSplashLeft.transform.parent = transform;
            //largeLavaSplashRight.transform.parent = transform;

            Destroy(largeLavaSplashLeft, 0.3f);
            Destroy(largeLavaSplashRight, 0.3f);
        }
    }
}
