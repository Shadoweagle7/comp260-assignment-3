﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleDeath : MonoBehaviour {
    public int timer = 1;
    public int count = 1;
    public int TimeTillDeath = 2;
	// Use this for initialization
	void Start () {
        InvokeRepeating("death", timer, timer);
	}
	
	// Update is called once per frame
	void death () {
		if(count ==  TimeTillDeath)
        {
            Destroy(this.gameObject);
        }
        count++;
	}
}
