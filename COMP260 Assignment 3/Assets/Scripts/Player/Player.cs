﻿using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class Player : MonoBehaviour {

    public AudioClip PointClip;
    public AudioClip JumpClip;
    public AudioSource PlayerAudioSource;
    
   
    public Rigidbody2D leftWall;
    public Rigidbody2D rightWall;
    private Rigidbody2D playerRigidbody;
    public Rigidbody2D spawnPoint;

    public float HorizontalSpeed = 0f, VerticalSpeed = 0f, 
        Acceleration = 1f, MaxSpeed = 5f, Brake = 5f, 
        MaxJump = 5f, JumpCount = 50f, JumpCountOrig = 50f;

    public bool JumpEnd = false, DoubleJump = true;

    private bool collidingWithGround;
    private bool begin = false; //determines whether to start the score
    //ui for score
    public Text scoreText;
    public int scoreCount;
    public int scoreReseter; //because im lazy it figures out when to add to the scoreCount
    public int HighScore;

    private void Move()
    {
        float x = Input.GetAxis("Horizontal"), y = Input.GetAxis("Vertical");

        // Movement

        if (x > 0f) // To the right
        {
            this.HorizontalSpeed += this.Acceleration;
            
        }
        else if (x < 0f) // To the left
        {
            this.HorizontalSpeed -= this.Acceleration;
        }
        else // Braking
        {
            if (this.HorizontalSpeed > 0f)
            {
                if (this.HorizontalSpeed - this.Brake * Time.deltaTime < 0f)
                {
                    this.HorizontalSpeed = 0f;
                }
                else
                {
                    this.HorizontalSpeed = this.HorizontalSpeed - this.Brake * Time.deltaTime;
                }
            }
            else if (this.HorizontalSpeed < 0f)
            {
                if (this.HorizontalSpeed + this.Brake * Time.deltaTime > 0f)
                {
                    this.HorizontalSpeed = 0f;
                }
                else
                {
                    this.HorizontalSpeed = this.HorizontalSpeed + this.Brake * Time.deltaTime;
                }
            }
        }

        if (y > 0f) // Upward
        {
 
            if (this.VerticalSpeed + 1 >= MaxJump)
            {
               
                if (DoubleJump == false)
                {
                    JumpEnd = true;
                }
                else
                {
                  
                    this.VerticalSpeed = 0;
                    DoubleJump = false;
                }
            }
            if (!JumpEnd)
            {

                this.VerticalSpeed += this.Acceleration;
                this.PlayerAudioSource.PlayOneShot(this.JumpClip);
            }
        }
        else if (y < 0f) // Downward
        {
            this.VerticalSpeed -= this.Acceleration;
        }
        
        if (JumpEnd)
        {
            
            //if(collidingWithGround == true)
            //{
            //    JumpEnd = false;
            //}
            //JumpCount -= 1;
            //if (JumpCount <= 0)
            //{
            //    JumpEnd = false;
            //    JumpCount = JumpCountOrig;
            //}
        }

        this.HorizontalSpeed = Mathf.Clamp(this.HorizontalSpeed, -1 * this.MaxSpeed, this.MaxSpeed);
        this.VerticalSpeed = Mathf.Clamp(this.VerticalSpeed, -1 * this.MaxJump, this.MaxJump);
        //this.VerticalSpeed = Mathf.Clamp(this.VerticalSpeed, -1 * this.MaxSpeed, this.MaxSpeed);
        Vector2 velocity = (Vector2.right * this.HorizontalSpeed) + (Vector2.up * this.VerticalSpeed);

        this.playerRigidbody.velocity = velocity;

        // Gravity
        if (!this.collidingWithGround)
        {
            this.VerticalSpeed -= this.playerRigidbody.gravityScale;
        }
    }

	// Use this for initialization
	void Start () {

        scoreText.text = "Score: 0";
        this.playerRigidbody = GetComponent<Rigidbody2D>();
        this.collidingWithGround = false;
        this.HighScore = 0;
        this.scoreCount = 0;
        this.PlayerAudioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        //deals with making the walls transparent and solid etc
        if (begin)
        {
            rightWall.GetComponent<BoxCollider2D>().enabled = true; //turns on collider (allowing for deaths)
            Color tmp = leftWall.GetComponent<SpriteRenderer>().color;
            tmp.a = 0.99f; //makes the wall opaque, can be changed
            rightWall.GetComponent<SpriteRenderer>().color = tmp;
            leftWall.GetComponent<BoxCollider2D>().enabled = true; //turns on collider (allowing for deaths)
            leftWall.GetComponent<SpriteRenderer>().color = tmp;

            //builds up the score after 30 runs of 'update' and displays the new value
            if (scoreReseter == 30)
            {
                scoreCount += 1;
                scoreReseter = 0;
            }
            scoreReseter += 1;
        }
        else
        {
            leftWall.GetComponent<BoxCollider2D>().enabled = false;
            Color tmp = leftWall.GetComponent<SpriteRenderer>().color;
            tmp.a = 0f; //sets the opacity of the laser
            leftWall.GetComponent<SpriteRenderer>().color = tmp;
            rightWall.GetComponent<BoxCollider2D>().enabled = false;
            leftWall.GetComponent<SpriteRenderer>().color = tmp;
        }
        scoreText.text = "Score: " + scoreCount + " | High Score: " + this.HighScore;
        this.Move();

        if (this.scoreCount > this.HighScore)
        {
            this.HighScore = this.scoreCount;
        }

        if (this.transform.position.y < -30) // Prevent falling off of map.
        {
            this.transform.position = spawnPoint.position;
            begin = false;
            this.scoreCount = 0;
        }
	}

    private void OnCollisionEnter2D(Collision2D collision2D)
    {
        // Replace the string the Contains() function with the name of the deadly boxes

        //if the player touches one of the falling points,
        //destroys the object and adds points
        if (collision2D.gameObject.name.Contains("Point")) // Get points
        {
            this.PlayerAudioSource.PlayOneShot(PointClip);
            PointBehaviour point = collision2D.gameObject.GetComponent<PointBehaviour>();
            this.scoreCount += point.ScoreReward;
            Destroy(collision2D.gameObject);

        }

        // The following things kill the player
        else if(collision2D.gameObject.name.Contains("Laser") 
            || collision2D.gameObject.name.Contains("Circular Saw"))
        {
            this.transform.position = spawnPoint.position;
            begin = false;
            this.scoreCount = 0;
        }
        if(collision2D.gameObject.name.Contains("Floor"))
        {
            begin = false;
        }
        if(collision2D.gameObject.name.Contains("Starting"))
        {
            JumpEnd = false;
            DoubleJump = true;
            begin = false;
        }
        if (collision2D.gameObject.name.Contains("Box")) //box contact resets jump
        {
            if(!begin)
            {
                begin = true;
            }
            if (this.transform.position.y > collision2D.transform.position.y)
            {
                JumpEnd = false;
                DoubleJump = true;
                
                this.collidingWithGround = true;
                //JumpEnd = false;
            }
        }
    }

    private void OnCollisionExit2D(Collision2D collision2D)
    {
        this.collidingWithGround = false;
        //JumpEnd = true;
    }
}
