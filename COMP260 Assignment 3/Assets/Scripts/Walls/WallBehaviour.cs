﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallBehaviour : MonoBehaviour {
    public Rigidbody2D avatar;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
        float x = this.gameObject.transform.position.x;
        float y = avatar.gameObject.transform.position.y;
        this.transform.position = new Vector2(x, y);
    }
}
