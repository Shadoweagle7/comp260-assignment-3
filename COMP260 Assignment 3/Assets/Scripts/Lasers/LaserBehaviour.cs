﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBehaviour : MonoBehaviour {
    int timer = 1;
    public int TimeTillDeath = 1;
    public int TimeTillFullLaser = 2;
    private bool playerKill = false;
    int count = 0;

    public AudioClip laserChargeClip;
    public AudioClip laserShotClip;
    private AudioSource audioCharge;
    private AudioSource audioShot;
    private AudioSource audio;

    public Rigidbody2D spawnPoint;
    public Player player;
	// Use this for initialization
	void Start () {
        audio = GetComponent<AudioSource>();
        audio.PlayOneShot(laserChargeClip);
        //disables the collider when the laser isn't 'fully charged' and also gives it a lower opacity
        this.GetComponent<BoxCollider2D>().enabled = false;
        Color tmp = this.GetComponent<SpriteRenderer>().color;
        tmp.a = 0.3f; //sets the opacity of the laser
        this.GetComponent<SpriteRenderer>().color = tmp;

        //repeatedly calls this function
        InvokeRepeating("Death", timer, timer);

    }
	
	// this is called once per second or whatever the timer is set to
	void Death () {
        count++;
		if (count == TimeTillFullLaser)
        {
            audioShot = GetComponent<AudioSource>();
            audioShot.PlayOneShot(laserShotClip);
            //after the laser is 'charged' it becomes fully opaque, signalling it killing the player
            this.GetComponent<BoxCollider2D>().enabled = true; //turns on collider (allowing for deaths)
            Color tmp = this.GetComponent<SpriteRenderer>().color; 
            tmp.a = 0.99f; //makes the laser opaque, can be changed
            this.GetComponent<SpriteRenderer>().color = tmp;
            //playerKill = true;
        } else if(count == TimeTillDeath)
        {
            Destroy(this.gameObject);
        }
	}
    //ignore this stuff as it was moved into 'Player' for convenience
    private void OnCollisionEnter2D(Collision2D collision2D)
    {
        if (playerKill == true)
        {
            if (collision2D.gameObject == this.player.gameObject)
            {
                //this.player.gameObject.transform.position.Set(-16.5f, 7.13f, 0);
                // Deal with death properly
                //collision2D.transform.position = spawnPoint.position;
                //Destroy(collision2D.gameObject);
            }
        }

    }
}
