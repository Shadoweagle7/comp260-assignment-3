﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserSpawner : MonoBehaviour {

    public float timer = 3f;
    public LaserBehaviour LaserPrefab;
    public int nlaser = 3;
    public Rect spawnRect;
    public int count = 0;

    
    public Rigidbody2D rigidbodyPlayer;
    
    void Start()
    {
        InvokeRepeating("Spawner", timer, timer);
    }

    //spawns in the boxes that will occur once every 'timer' has passed above the player
    void Spawner()
    {
        //taken from week 4 practical
        // instantiate a laser
        LaserBehaviour laser = Instantiate(LaserPrefab);
        // attach to this object in the hierarchy
        laser.transform.parent = transform;
        // give the box a name and number
        laser.gameObject.name = "Laser " + count++;
        // move the box to a random position within
        // the spawn rectangle at 13 spaces above the player
        float x = spawnRect.xMin +
        Random.value * spawnRect.width;
        float y = rigidbodyPlayer.position.y + 20;//spawnRect.yMin +
        //Random.value * spawnRect.height;
        laser.transform.position = new Vector2(x, y);
    }
    void OnDrawGizmos()
    {
        // draw the spawning line
        
        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(
            new Vector2(spawnRect.xMin, rigidbodyPlayer.position.y + 15),
            new Vector2(spawnRect.xMax, rigidbodyPlayer.position.y + 15));
    }
}
