﻿using UnityEngine;

[RequireComponent(typeof(PolygonCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class CircularSawBehaviour : MonoBehaviour {
    public Vector2 Direction;
    public float MaxDistanceTravel;
    public float Speed = 1f;
    private Rigidbody2D CircularSawRigidBody2D;

    // Use this for initialization
    void Start () {
        this.CircularSawRigidBody2D = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Mathf.Abs(this.transform.position.x) > this.MaxDistanceTravel)
        {
            Destroy(this.gameObject);
        }

        this.CircularSawRigidBody2D.velocity = this.Direction * this.Speed;
    }

    private void OnCollisionEnter2D(Collision2D collision2D)
    {
        if (!collision2D.gameObject.name.Equals("Player"))
        {
            // Ignore the collision somehow. Physics.IgnoreCollision doesn't work here because
            // the function only accepts Colliders, not Collider2Ds. I can't remember how to do this...
        }
    }
}
