﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircularSawSpawner : MonoBehaviour {

    public CircularSawBehaviour CircularSawBehaviourPrefab;
    public int SpawnGroupCount = 3;
    public int SpawnFrequencyInSeconds = 3;
    public Rigidbody2D PlayerObject;
    private CircularSawBehaviour[] CircularSaws;
    private float TimeSinceLastSpawn = 0f;
    public Rect SpawnArea;
    public int delayTillSpawn = 0;
    private int Count = 0;

    private void Spawn()
    {
        //if(player.gameObject.GetComponent<scoreCount> == 0)

        this.TimeSinceLastSpawn = 0f;

        for (int i = 0; i < this.SpawnGroupCount; i++)
        {
            this.CircularSaws[i] = Instantiate<CircularSawBehaviour>(this.CircularSawBehaviourPrefab);
            this.CircularSaws[i].MaxDistanceTravel = this.SpawnArea.width;

            int sideSpawned = Random.Range(0, 2);

            if (sideSpawned == 0)
            {
                this.CircularSaws[i].Direction = Vector2.right;
            }
            else if (sideSpawned == 1)
            {
                this.CircularSaws[i].Direction = Vector2.left;
            }

            float x = this.SpawnArea.xMin + sideSpawned * this.SpawnArea.width;
            float y = this.PlayerObject.position.y + Random.Range(-4f, 4f);
            if(y < 2 && y > -2)
            {
                y = 4.5f;
            }


            this.CircularSaws[i].Speed = 10f;
            this.CircularSaws[i].transform.position = new Vector2(x, y);
            this.CircularSaws[i].transform.parent = transform;
            this.CircularSaws[i].name = "Circular Saw " + this.Count++;
        }
    }

	// Use this for initialization

	void Start () {
        if (this.SpawnGroupCount <= 0)
        {
            this.CircularSaws = null;
        }
        else
        {
            this.CircularSaws = new CircularSawBehaviour[this.SpawnGroupCount];
        }
        //makes the saws only apper later in the game

        //Invoke("delay", delayTillSpawn);
        InvokeRepeating("Spawn", delayTillSpawn, this.SpawnFrequencyInSeconds);
    }
    /* trying to turn off collisions for other objectsthat aren't the player
    private void OnCollisionEnter2D(Collision2D collision2D)
    {
        if (!collision2D.gameObject.name.Contains("Player"))
        {
            

        }
        
    }
    */

    // Update is called once per frame
    void Update () {

    }
}
