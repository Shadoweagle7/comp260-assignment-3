﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutTextDestroy : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Invoke("death", 5);
	}
	
	// Update is called once per frame
	void death () {
        Destroy(this.gameObject);
	}
}
