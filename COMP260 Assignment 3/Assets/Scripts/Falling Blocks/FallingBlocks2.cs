﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingBlocks2 : MonoBehaviour {
    public Player boxPrefab;
    public int nBoxes = 3;
    public Rect spawnRect;
    // Use this for initialization
    void Start () {
        for (int i = 0; i < nBoxes; i++)
        {
            // instantiate a bee
            Player box = Instantiate(boxPrefab);
            // attach to this object in the hierarchy
            box.transform.parent = transform;
            // give the bee a name and number
            box.gameObject.name = "Box " + i;
            // move the bee to a random position within
            // the spawn rectangle
            float x = spawnRect.xMin +
            Random.value * spawnRect.width;
            float y = spawnRect.yMin +
            Random.value * spawnRect.height;
            box.transform.position = new Vector2(x, y);
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnDrawGizmos()
    {
        // draw the spawning rectangle
        Gizmos.color = Color.green;
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMin));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMin));
    }
}
