﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxGravity : MonoBehaviour {
    public int lowerRND = 1;
    public int higherRND = 3;
	// Use this for initialization
	void Start () {

        float temp = Random.Range(lowerRND, higherRND);
        this.GetComponent<Rigidbody2D>().gravityScale = temp;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
