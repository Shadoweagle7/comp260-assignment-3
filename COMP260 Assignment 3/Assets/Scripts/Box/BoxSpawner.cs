﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxSpawner : MonoBehaviour {
    public float timer = 0.5f;
    public GameObject boxPrefab;
    public int nBoxes = 3;
    public Rect spawnRect;
    public int count = 0;
    public int reduceSpawnRate = 30; //determines when to slow down the spawning of boxes to increase difficulty

    public int lowerRND = 1;
    public int higherRND = 3;
    // Use this for initialization
    public Rigidbody2D rigidbodyPlayer;
    void Start () {
        
        Invoke("Spawner", timer);
	}
	
	//spawns in the boxes that will occur once every 'timer' has passed above the player
	void Spawner () {
        //taken from week 4 practical
        // instantiate a box

        //boxes spawn rate decreases a tiny bit each time count mod reducespawnrate is 0 until a certain count
        
        if (count < 200)
        {
            if (count % reduceSpawnRate == 0)
            {
                timer += 0.1f;
            }
        }
        
        GameObject box = Instantiate(boxPrefab);
        //randomly generates a number for the gravity of the box
        float temp = Random.Range(lowerRND, higherRND);
        box.GetComponent<Rigidbody2D>().gravityScale = temp;

        // attach to this object in the hierarchy
        box.transform.parent = transform;
        // give the box a name and number
        box.gameObject.name = "Box " + count++;
        // move the box to a random position within
        // the spawn rectangle at 13 spaces above the player
        float x = spawnRect.xMin +
        Random.value * spawnRect.width;
        float y = rigidbodyPlayer.position.y + 12;//spawnRect.yMin +
        //Random.value * spawnRect.height;
        box.transform.position = new Vector2(x, y);
        Invoke("Start", timer);
    }
    void OnDrawGizmos()
    {
        // draw the spawning line
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(
            new Vector2(spawnRect.xMin, rigidbodyPlayer.position.y + 13),
            new Vector2(spawnRect.xMax, rigidbodyPlayer.position.y + 13));
            
            /*
         new Vector2(spawnRect.xMin, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMin));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMin));
         */
    }
}
